import React from "react";

import { StyleSheet, Text, View, Image, TouchableOpacity, SafeAreaView, ScrollView } from "react-native";

import Icon from 'react-native-vector-icons/Entypo';

import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import FIcon from 'react-native-vector-icons/FontAwesome5';


const App = () => {

  return (

    <View style={{ backgroundColor: "#32418f", flex: 1, }}>

      <View style={{ flexDirection: "row" }}>


        <Text style={{ fontSize: 25, textAlign: "center", marginLeft: 180, color: "white", marginTop: 15 }}>Pay</Text>

        <TouchableOpacity>
          <Text style={{ textAlign: "right", color: "white", marginLeft: 130, marginTop: 20 }}>
            <Icon name="dots-three-vertical" size={15} color="white" /> </Text>
        </TouchableOpacity>

      </View>

      <View>
        <TouchableOpacity>
          <Image source={require('./assets/cashback.png')} style={{ height: 100, width: 300, marginLeft: 50, marginTop: 15, borderColor: "white" }} />
        </TouchableOpacity>
      </View>

      {/* container */}

      <ScrollView>
        <View>

          <View style={style.container}>
            <View style={{ flexDirection: "row" }}></View>

            <View style={{ marginLeft: -7 }}>

              <TouchableOpacity>
                <View style={style.container1}>

                  <Text style={{ justifyContent: 'center', padding: 5 }} > <MIcon name="cellphone-charging" size={50} color="#ff4493" /></Text>

                  <Text style={style.containertext1}>Mobile</Text>

                </View>
              </TouchableOpacity>

              <TouchableOpacity>
                <View style={style.container2}>

                  <Text style={{ justifyContent: 'center', padding: 5 }} > <Icon name="landline" size={50} color="#1b95ff" /></Text>

                  <Text style={style.containertext2}>Landline</Text>

                </View>
              </TouchableOpacity>

              <TouchableOpacity>
                <View style={style.container3}>

                  <Text style={{ justifyContent: 'center', padding: 5 }} > <MIcon name="usb-flash-drive" size={50} color="#926be8" /></Text>

                  <Text style={style.containertext3}>Datacard</Text>

                </View>
              </TouchableOpacity>

              <View style={{ flexDirection: "row" }}>

                <TouchableOpacity>
                  <View style={style.container4}>


                    <Text style={{ justifyContent: 'center', padding: 5 }} > <FIcon name="wifi" size={50} color="#2eb8f4" /></Text>
                    <Text style={{ paddingTop: -50, marginLeft: 1 }}></Text>
                    <Text style={style.containertext4}>Internet</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container5}>

                    <Text style={{ justifyContent: 'center', padding: 5 }}  > <FIcon name="satellite-dish" size={50} color="#01b0c9" /></Text>

                    <Text style={style.containertext5}>Cable Tv</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container6}>

                    <Text style={{ justifyContent: 'center', padding: 5 }} > <Icon name="clapperboard" size={50} color="#ef5c58" /> </Text>

                    <Text style={style.containertext6}>Entertainment</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container7}>


                    <Text style={{ justifyContent: 'center', padding: 5 }} > <FIcon name="lightbulb" size={50} color="#ffa518" /> </Text>
                    <Text style={{ paddingTop: -50, marginLeft: 1 }}></Text>
                    <Text style={style.containertext7}>Electric</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container8}>

                    <Text style={{ justifyContent: 'center', padding: 5 }} > <Icon name="open-book" size={50} color="#fd7d5a" />  </Text>

                    <Text style={style.containertext8}>School</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container9}>

                    <Text style={{ justifyContent: 'center', padding: 5 }} > <FIcon name="church" size={50} color="#01b1ca" /> </Text>

                    <Text style={style.containertext9}> Church</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container10}>


                    <Text style={{ justifyContent: 'center', padding: 5 }} > <FIcon name="donate" size={50} color="#fd5097" /></Text>
                    <Text style={style.containertext10}>Donation</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container11}>

                    <Text style={{ justifyContent: 'center', padding: 5 }} > <FIcon name="shopping-bag" size={50} color="#1b95ff" /> </Text>

                    <Text style={style.containertext11}>Merchants</Text>

                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={style.container12}>

                    <Text style={{ justifyContent: 'center', padding: 5 }} > <FIcon name="tools" size={50} color="#926be8" /></Text>

                    <Text style={style.containertext12}> Utility</Text>

                  </View>
                </TouchableOpacity>

              </View>

            </View>

          </View>

        </View>
      </ScrollView>


{/* bottom tabs */}

  <View style={{ flexDirection: "row", backgroundColor: "white" }}>
        <TouchableOpacity>
        <FIcon name="money-check-alt" size={25} color="grey" style={{ marginLeft: 40 }} />
        </TouchableOpacity>
        <Text style={style.bottom1}>Balance</Text>
        <TouchableOpacity>
        <Icon name="paypal" size={25} color="grey" style={{ marginLeft: 40 }} />
        </TouchableOpacity>
        <Text style={style.bottom2}>Pay</Text>
        <TouchableOpacity>
        <MIcon name="send-circle" size={25} color="grey" style={{ marginLeft: 50 }} />
        </TouchableOpacity>
        <Text style={style.bottom3}>Send</Text>
        <TouchableOpacity>
        <MIcon name="history" size={25} color="grey" style={{ marginLeft: 40 }} />
        </TouchableOpacity>
        <Text style={style.bottom4}>History</Text>
        <TouchableOpacity>
        <MIcon name="more" size={25} color="grey" style={{ marginLeft: 30 }} />
        </TouchableOpacity>
        <Text style={style.bottom5}>More</Text>
   </View>


    </View>
  );
};


const style = StyleSheet.create({

  container: {
    backgroundColor: "white",
    flex: 1
  },
  container1: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginVertical: 5,
    marginTop: 30,
    marginHorizontal: 16,
    justifyContent: 'center',
    borderLeftColor: "white",
    borderWidth: 0.19,
    borderColor: "#dcdcdc"
  },

  container2: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginVertical: 15,
    marginHorizontal: 141,
    marginTop: -130,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },

  container3: {
    width: 125,
    height: 125,
    flexDirection: "row",
    backgroundColor: 'white',
    padding: 5,
    marginVertical: 15,
    marginHorizontal: 140,
    marginLeft: 265,
    marginTop: -140,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: '#dcdcdc'
  },

  container4: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginVertical: 15,
    marginHorizontal: 160,
    marginLeft: 16,
    marginTop: -16,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },


  container5: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,

    marginHorizontal: 150,
    marginLeft: -160,
    marginTop: -16,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },

  container6: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginHorizontal: 150,
    marginLeft: -151,
    marginTop: -16,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },

  container7: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginVertical: 150,
    marginHorizontal: 160,
    marginLeft: -524,
    marginTop: 108,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },

  container8: {
    width: 125,
    height: 125,
    flexDirection: "row",
    backgroundColor: 'white',
    padding: 5,
    marginHorizontal: 150,
    marginLeft: -399,
    marginTop: 108,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },


  container9: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginHorizontal: 150,
    marginLeft: -275,
    marginTop: 108,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },

  container10: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginVertical: 150,
    marginHorizontal: 160,
    marginLeft: -524,
    marginTop: 232,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: '#dcdcdc'
  },

  container11: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,

    marginHorizontal: 150,
    marginLeft: -399,
    marginTop: 232,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: 'grey'
  },

  container12: {
    width: 125, height: 125, flexDirection: "row", backgroundColor: 'white',
    padding: 5,
    marginHorizontal: 150,
    marginLeft: -275,
    marginTop: 232,
    justifyContent: 'center',
    borderWidth: 0.19,
    borderColor: '#dcdcdc',
  },

  containertext1: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -50 },

  containertext2: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -50 },

  containertext3: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -50 },

  containertext4: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -60 },

  containertext5: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 74, marginLeft: -60 },

  containertext6: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -80 },

  containertext7: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -50 },

  containertext8: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -60 },

  containertext9: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -70 },

  containertext10: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -60 },

  containertext11: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -60 },

  containertext12: { alignItems: "center", direction: "center", paddingBottom: 10, paddingTop: 70, marginLeft: -60 },


  bottom1: { fontSize: 12, marginTop: 30, marginLeft: -35 },
  bottom2: { fontSize: 12, marginTop: 30, marginLeft: -25},
  bottom3: { fontSize: 12, marginTop: 30, marginLeft: -25 },
  bottom4: { fontSize: 12, marginTop: 30, marginLeft: -30 },
  bottom5: { fontSize: 12, marginTop: 30, marginLeft: -20 },

})


export default App;